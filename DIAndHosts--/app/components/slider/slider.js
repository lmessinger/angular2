import {Component, View,CORE_DIRECTIVES} from 'angular2/angular2';
import {RouterLink} from 'angular2/router';
import {ImageList,Image} from '../../services/image_list';

@Component({
  selector: 'imageslider'
})
@View({
  templateUrl: './components/slider/slider.html',
  directives: [RouterLink,CORE_DIRECTIVES]
})
export class ImageSlider {
  constructor( list:ImageList) {
    this.list = list;
    console.log('listslider',list);
  }

}
