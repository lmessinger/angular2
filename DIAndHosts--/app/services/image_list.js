import {CORE_DIRECTIVES} from 'angular2/angular2';

export class Image {
  constructor(url,score) {
    this.src = url;
    this.score = score;
  }
}

//@Injectable
export class ImageList {

  constructor() {
    this.images = [
              {
              src: "images/calm-summer-gif.gif?b91b38",
              score: 1752
              },
              {
              src: "images/animated-street-art.gif",
              score: 66
              },
              {
              src: "images/disco-kitty-animated-gif-arts.gif",
              score: 88
              },
              {
              src: "images/photography-gif-art.gif?b91b38",
              score: 57
              },
              {
              src: "images/newspaper-flip-animation-photo-art.gif?b91b38",
              score: 56
              },
              {
              src: "images/cinemagraph-girl-hair-art.gif?b91b38",
              score: 77
              },
              {
              src: "images/leg-move-photograph-animation-art.gif?b91b38",
              score: 12
              },
              {
              src: "images/creepy-eye-movement-cinemagraph.gif?b91b38",
              score: 67
              },
              {
              src: "images/cinemagraph-dancing-art.gif?b91b38",
              score: 99
              },
              {
              src: "images/a_little_over_8_years_ago___pt_1_by_clayrodery-d507v7h.gif?b91b38",
              score: 63
              },  {
                src: "images/classic-cinemagraph-hair-clothing-moving-guy-photo-art.gif?b91b38",
                score: 72
                }, {
                  src: "images/awesome-still-photo-earring-animation-art-gif.gif",
                  score: 71
                  },

            ];

          }
  get(): Image[] {
    return this.images;
  }
  add(imageObj:Image): void {
    this.images.push(imageObj);
  }
}
