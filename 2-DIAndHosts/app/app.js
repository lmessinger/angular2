import {Component, View, bootstrap} from 'angular2/angular2';
import {routerInjectables,RouteConfig, RouterOutlet, RouterLink} from 'angular2/router';
import {Http,HTTP_BINDINGS} from 'http/http';

import {ImageSlider} from './components/slider/slider';
import {ImageManager} from './components/manager/manager';
import {Preview} from './components/preview/preview';
import {ImageList} from './services/image_list'
@Component({
  selector: 'app'

})
// LM==>4 now let's see what's all about routing. but first: what is routing?
//
// Configure routing
// /about will call the about component
@RouteConfig([
  { path: '/', component: ImageSlider, as: 'slider' },
  { path: '/manager', component: ImageManager, as: 'manager' } ,
  { path: '/preview', component: Preview, as: 'preview' } ,
  { path: '/slider', component: ImageSlider, as: 'slider' }
])
// LM==>5 the RouterOutlet is where the new partial will come in
@View({
  templateUrl: './app.html',
  directives: [RouterOutlet, RouterLink]
})
class App {



}


bootstrap(App, [routerInjectables,HTTP_BINDINGS,ImageList]);
