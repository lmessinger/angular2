import {Component, View, CORE_DIRECTIVES,Inject} from 'angular2/angular2';

import {ImageList,Image} from '../../services/image_list';

@Component({


  selector: 'imagemanager'
})
@View({
  templateUrl: './components/manager/manager.html',
  directives: [CORE_DIRECTIVES]
})
export class ImageManager {
  constructor( list:ImageList) {
    this.list = list;
  }
  addName(newurl,score): boolean {
    this.list.add(new Image(newurl.value,score.value));
    //newurl.value='';
    //score.value = null;
    // prevent default form submit behavior to refresh the page
    return false;
  }
}
