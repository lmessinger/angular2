import {Component, View,CORE_DIRECTIVES,NgIf } from 'angular2/angular2';
import {ImageSlider} from '../slider/slider';
import {ImageManager} from '../manager/manager';

@Component({
  selector: 'preview',
  host: {
    '(^mousedown)': 'keyupHandler($event)'
  }
})
@View({
  template: `<imageslider [play]="playPreview" class="col-xs-6"></imageslider>
  <div class="col-xs-1">
    <span *ng-if="playPreview===true">||></span>
  </div>
  <imagemanager class="col-xs-5" ></imagemanager>`,
  directives: [ImageSlider,ImageManager,NgIf]
})
export class Preview {
  constructor() {
    this.playPreview = true;
  }

  keyupHandler($event) {
    this.playPreview = !this.playPreview;
  }

}
