import {Component, View, bootstrap} from 'angular2/angular2';
import {routerInjectables,RouteConfig, RouterOutlet, RouterLink} from 'angular2/router';
import {Http,HTTP_BINDINGS} from 'http/http';

import {Home} from './components/home/home';
import {Entities} from './components/entities/entities';
import {NameList} from './services/name_list'
@Component({
  selector: 'app'
})
// LM==>4 now let's see what's all about routing. but first: what is routing?
//
// Configure routing
// /about will call the about component
@RouteConfig([
  { path: '/', component: Home, as: 'home' },
  { path: '/entities', component: Entities, as: 'entities' } ,
  { path: '/home', component: Home, as: 'home' }
])
// LM==>5 the RouterOutlet is where the new partial will come in
@View({
  templateUrl: './app.html?v=<%= VERSION %>',
  directives: [RouterOutlet, RouterLink]
})
class App {}


bootstrap(App, [routerInjectables,HTTP_BINDINGS,NameList]);
