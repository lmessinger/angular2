import {CORE_DIRECTIVES,Injectable} from 'angular2/angular2';


//@Injectable
export class NameList {

  constructor() {
    this.names = ['Dijkstra', 'Knuth', 'Turing', 'Hopper'];
  }
  get(): string[] {
    return this.names;
  }
  add(value: string): void {
    this.names.push(value);
  }
}
