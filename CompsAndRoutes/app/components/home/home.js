import {Component, View} from 'angular2/angular2';
import {RouterLink} from 'angular2/router';

@Component({
  selector: 'home'
})
@View({
  templateUrl: './components/home/home.html?v=<%= VERSION %>',
  directives: [RouterLink]
})
export class Home {
  constructor( ) {
    console.log('home ctor');
    this.inhome = 1;
  }
}
