import {Component, View, CORE_DIRECTIVES,Inject} from 'angular2/angular2';

import {NameList} from '../../services/name_list';

@Component({

//  bindings: [NameList],
  selector: 'entities'
})
@View({
  templateUrl: './components/entities/entities.html',
  directives: [CORE_DIRECTIVES]
})
export class Entities {
  constructor( @Inject(NameList) list) {
    this.list = list;
  }
  addName(newname): boolean {
    this.list.add(newname.value);
    newname.value = '';
    // prevent default form submit behavior to refresh the page
    return false;
  }
}
