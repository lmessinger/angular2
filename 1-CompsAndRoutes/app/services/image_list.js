import {CORE_DIRECTIVES} from 'angular2/angular2';

export class Image {
  constructor(url,score) {
    this.url = url;
    this.score = score;
  }
}

//@Injectable
export class ImageList {

  constructor() {
    this.images = [];
  }
  get(): Image[] {
    return this.images;
  }
  add(imageObj:Image): void {
    this.images.push(imageObj);
  }
}
