import {Component, View,CORE_DIRECTIVES} from 'angular2/angular2';
import {RouterLink} from 'angular2/router';
import {ImageList,Image} from '../../services/image_list';

@Component({
  selector: 'imageslider',
  properties: ['play']
})
@View({
  templateUrl: './components/slider/slider.html',
  directives: [RouterLink,CORE_DIRECTIVES]
})
export class ImageSlider {
  constructor( list:ImageList) {
    // wait for return
    list.getImages((data)=>{
      this.list = data.images;
      console.log('listslider',list);
      this.interval = 2000;
      this.currIndex = 0;
      this.currentImg = this.list[this.currIndex++];
      this.startTimer();
      this.play = true;
    });

  }

  startTimer() {
    var intervalID = setInterval(() => {
      if (this.play) {
          this.currentImg = this.list[this.currIndex++];
          this.currIndex = ((this.currIndex >= this.list.length) ? 0 : this.currIndex);
      }
    },this.interval);
  }

}
