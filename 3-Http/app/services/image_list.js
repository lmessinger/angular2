import {CORE_DIRECTIVES} from 'angular2/angular2';
import {Http,HTTP_BINDINGS} from 'http/http';
export class Image {
  constructor(url,score) {
    this.src = url;
    this.score = score;
  }
}

//@Injectable
export class ImageList {

  constructor(http:Http) {
     console.log('in db ctor');

    this.images = [];

    this.http = http;


  }
  get(): Image[] {
    return this.images;
  }
  add(imageObj:Image): void {
    this.images.push(imageObj);
  }

  getImages(cb) {
  console.log('db getEntities');

  this.http.get('./images.json')
   //Get the RxJS Subject
   .toRx()
   // Call map on the response observable to get the parsed  object
   .map(function(res) {
       return  res.json()}
       )
   // Subscribe to the observable to get the parsed object and send it to the callback
   .subscribe(
     cb);

}

}
